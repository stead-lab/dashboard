const config = {
  dbName: 'stead'
};

const utils = {
  showNotification: (title, options) => Notification.permission === 'granted' ? self.registration.showNotification(title, options) : null,
  sendNotificationData: (message, client = self) => client.postMessage(message)
};

const MessageHandler = (ev) => {
  if (!ev.data || typeof ev.data.json !== 'function') return;

  if (ev.data.eventType === 'ping') {
    console.debug('Service worker ping received');
    return;
  }

  const data = ev.data.json();
  if (!data.type || !data.content) return;

  const {type, content} = data;
  switch (type) {
    case 'notification':
      if (content && content.body && content.title) {
        const {title, ...options} = content;
        ev.waitUntil(utils.showNotification(title, options));
      } else {
        console.error('Invalid Notification: A body and title are required.');
      }
      break;
    default:
      console.debug(`Unsupported message type ${data.type}`);
  }
};

const NotificationHandler = (ev) => {
  const action = ev.action;
  const data = ev.notification.data;
  const opts = Object.assign({
    destinationURL: '/',
    dismissAct: 'dismiss'
  }, data.options);

  ev.notification.close();

  // Do nothing if user selected the dismiss action
  if (action === opts.dismissAct) return;

  /* Check if any clients exist with the proposed destinationUrl,
   if none, open a new client window */
  ev.waitUntil(clients.matchAll({type: 'window'})
    .then(function (list) {
      for (let i = 0; i < list.length; i++) {
        const client = list[i];
        let destination = opts.destinationURL;
        const url = new URL(client.url);
        const message = {type: 'notificationClick', action, data};

        if (url.pathname === destination && 'focus' in client) {
          return client.focus().then((cl) => {
            if (action) utils.sendNotificationData(message, cl);
          }).catch(console.debug);
        } else if (clients.openWindow) {
          return clients.openWindow(destination).then((win) => {
            if (win) return win.focus().then(cl => {
              if (action) utils.sendNotificationData(message, cl);
            }).catch(console.debug);
          });
        }
      }
    }));
};

self.addEventListener('install', () => self.skipWaiting());

self.addEventListener('push', MessageHandler);
self.addEventListener('message', MessageHandler);
self.addEventListener('notificationclick', NotificationHandler);

/* WORKBOX STUFF */

if (workbox) {
  const dbName = config.dbName;
  workbox.setConfig({debug: false});
  workbox.core.setCacheNameDetails({prefix: dbName, suffix: false});

  workbox.precaching.precacheAndRoute(self.__precacheManifest);
  workbox.core.skipWaiting();
  workbox.core.clientsClaim();

  workbox.routing.setCatchHandler(err => console.debug(err.message || err.err.message || err));
  workbox.routing.setDefaultHandler(new workbox.strategies.NetworkOnly());

  // Cache any resources for 20 days before expiring them
  workbox.routing.registerRoute(/\.(?:jpeg|jpg|gif|png|svg|woff|woff2)/g,
    new workbox.strategies.CacheFirst({
      cacheName: `${dbName}-static`,
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 60,
          maxAgeSeconds: 20 * 24 * 60 * 60
        })
      ]
    }));
} else {
  console.debug(`Workbox failed to load 😬`);
}
