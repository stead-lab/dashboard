import { VueConstructor } from 'vue';
import { slugify } from '@/utils';

interface IGeneralMixin {
  drk: boolean;
  mob_mq: MediaQueryList | null;
  drk_mq: MediaQueryList | null;
  isOnline: boolean;
  isMobile: boolean;
}

export function registerGlobalMixin(Vue: VueConstructor) {
  const extension = Vue.extend<IGeneralMixin, any, any>({
    data() {
      return {
        drk: true,
        mob_mq: null,
        drk_mq: null,
        isOnline: true,
        isMobile: false
      };
    },
    mounted() {
      if (typeof window !== 'undefined') {
        this.mob_mq = window.matchMedia('(max-width: 740px)');
        this.drk_mq = window.matchMedia('(prefers-color-scheme: dark)');

        const onlineHandler = () => this.isOnline = true;
        const offlineHandler = () => this.isOnline = false;
        const isMobileHandler = () => this.isMobile = this.mob_mq.matches;

        isMobileHandler();
        this.prefersDark();
        window.addEventListener('online', onlineHandler);
        window.addEventListener('offline', offlineHandler);
        if (this.mob_mq) this.mob_mq.addEventListener('change', isMobileHandler);

        this.$once('hook:beforeDestroy', () => {
          window.removeEventListener('online', onlineHandler);
          window.removeEventListener('offline', offlineHandler);
          if (this.mob_mq) this.mob_mq.removeEventListener('change', isMobileHandler);
        });
      }
    },
    methods: {
      errorHandler(err: string | { code?: string, message?: string }): any {
        return this.$store.dispatch('notifications/handler', {isError: true, notice: err});
      },
      messageHandler(notice: string): any {
        return this.$store.dispatch('notifications/handler', {notice});
      },
      prefersDark() {
        // this.drk = this.drk_st.matches;
        // TODO: enable once support for light improves
        document.body.classList[this.drk ? 'add' : 'add']('drk');
        // document.body.classList[this.drk ? 'add' : 'remove']('drk');
      },
      duplicate(obj: object) {
        return Object.assign({}, obj);
      },
      slugify({target}: any, obj: any, key?: string) {
        const value = target.value;
        if (value) return obj[key || 'slug'] = slugify(value);
      },
      unSpace({target}: any, obj: any, key?: string) {
        const value = target.value;
        if (value) return obj[key || 'slug'] = value.replace(/ /g, '');
      },
      toArray({target}: any, obj: any, key: string) {
        const value = target.value;
        if (value) return obj[key] = value.replace(/ /g, '').split(',');
      }
    }
  });

  Vue.mixin(extension);
}
