import Vue from 'vue';
import Router from 'vue-router';
import { beforeHook } from '@/router/hooks';
import Home from '../pages/Dashboard/home.vue';
import Dashboard from '../pages/Dashboard/index.vue';

Vue.use(Router);

const Auth = () => import( /* webpackChunkName: "auth" */ '../pages/Auth/index.vue');
const SignUp = () => import( /* webpackChunkName: "auth" */ '../pages/Auth/sign-up.vue');
const SignIn = () => import( /* webpackChunkName: "auth" */ '../pages/Auth/sign-in.vue');
const ResetPassword = () => import(/* webpackChunkName: "auth" */ '../pages/Auth/reset.vue');

const Horses = () => import(/* webpackChunkName: "dashboard" */ '../pages/Dashboard/horses/index.vue');
const HorsesList = () => import(/* webpackChunkName: "dashboard" */ '../pages/Dashboard/horses/list.vue');
const HorsesView = () => import(/* webpackChunkName: "dashboard" */ '../pages/Dashboard/horses/view.vue');
const HorsesNew = () => import(/* webpackChunkName: "dashboard" */ '../pages/Dashboard/horses/new.vue');

const PageNotFound = () => import('../pages/Errors/404.vue');
const IssueOccurred = () => import('../pages/Errors/Issue.vue');

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/auth',
      component: Auth,
      children: [
        {
          path: '',
          name: 'auth',
          redirect: '/sign-in'
        },
        {
          path: '/sign-in',
          name: 'auth.sign-in',
          component: SignIn
        },
        {
          path: '/sign-up',
          name: 'auth.sign-up',
          component: SignUp
        },
        {
          path: '/reset',
          name: 'auth.reset',
          component: ResetPassword
        }
      ]
    },
    {
      path: '/',
      component: Dashboard,
      children: [
        {
          path: '',
          name: 'home',
          component: Home
        },
        {
          path: 'horses',
          component: Horses,
          children: [
            {
              path: '',
              name: 'horses',
              component: HorsesList
            },
            {
              path: 'view/:id',
              name: 'horses.view',
              component: HorsesView,
              props: true,
              meta: {
                checkExists: true
              }
            },
            {
              path: 'new',
              name: 'horses.new',
              component: HorsesNew
            }
          ]
        }
      ]
    },
    {
      path: '/issue',
      name: 'issue-occurred',
      component: IssueOccurred
    },
    {
      path: '/404',
      name: 'not-found',
      component: PageNotFound
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});

router.beforeEach(beforeHook);

export default router;
