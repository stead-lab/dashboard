import Store from '@/store';
import { Route } from 'vue-router';

declare const mixpanel: {
  track: (ev: string, obj: any) => void;
};

export function beforeHook(to: Route, from: Route, next: any): void {
  if (to.matched.some(route => route.meta.requiresAuth)) {
    Store.dispatch('load')
      .then((dest) => {
        // Store actions can also choose to redirect
        if (dest) return next(dest);

        // Redirect user if they do not meet some criteria
        const user = Store.state.auth.user;
        if (to.name?.indexOf('dashboard.') === -1 && !user?.onBoarded) {
          return next({name: 'auth.onboard'});
        }

        // Check if an item exists in the store
        if (to.matched.some(route => route.meta.checkExists)) {
          const groupName = (to.name as string).split('.')[0];
          const id = to.params.id;
          const notFound = 'No such item exists';

          const checkLocal = () => Store.dispatch(`${groupName}/fetchOneLocal`, {value: id}).then(() => next());

          checkLocal().catch(err => {
            if (err.message === notFound) {
              Store.dispatch(`${groupName}/fetchAll`)
                .then(checkLocal)
                .catch(err => {
                  if (err.message === notFound) {
                    // next({name: `${groupName}.list`});
                    next({name: 'not-found'});
                  } else next({name: 'issue-occurred'});
                });
            } else next({name: 'issue-occurred'});
          });
        }

        next();
      })
      .catch((err: { code: number, message: string }) => {
        // Redirect them if not authenticated
        if (err.code === 401) {
          next({name: 'auth'});
        } else {
          console.error(err);
          next({name: 'issue-occurred'});
        }
      });
  } else {
    next();
  }

  if (process.env.NODE_ENV === 'production' && to.matched.some(route => route.meta.track)) {
    if (typeof mixpanel !== 'undefined') {
      mixpanel.track('Page View', {
        Page: to.name,
        StoreName: to.params.store
      });
    }
  }
}
