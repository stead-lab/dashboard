import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import { init } from '@sentry/browser';
import VeeValidate from 'vee-validate';
import { Vue as VueSentry } from '@sentry/integrations';
import { registerGlobalMixin } from './utils/global.mixin';
import { registerGlobalFilter } from './utils/global.filter';
// SERVICE WORKER
// import './register.sw'

//  MIXINS, FILTERS AND PLUGINS
registerGlobalMixin(Vue);
registerGlobalFilter(Vue);
Vue.use(VeeValidate);

// VUE SETUP CONFIGURATIONS
Vue.config.productionTip = false;
new Vue({
  store,
  router,
  render: (h: any) => h(App)
}).$mount('main');

// SENTRY CONFIGURATION [PRODUCTION MAINLY]
if (process.env.NODE_ENV === 'production') {
  init({
    dsn: process.env.SENTRY_DSN,
    integrations: [new VueSentry({Vue})]
  });
}
