import { GetterTree, Module, MutationTree } from 'vuex';
import { IRootState, Storage } from '@/store/lib';
import * as types from './mutation.types';
import { IAuthState } from './auth.state';

import actions from './actions';

const getters: GetterTree<IAuthState, IRootState> = {
  user: ({user}) => user
};

const mutations: MutationTree<IAuthState> = {
  [types.LOAD_ADMIN](state, {user, token}) {
    state.user = user;
    state.token = token;
    state.isAuthed = true;
    Storage.set('$tkn', token);
  },
  [types.SIGN_OUT](state) {
    state.user = null;
    state.token = null;
    state.isAuthed = false;
    Storage.remove('$tkn');
  },
  [types.SET_VERIFIER](state, verifier) {
    state.phoneAuth.verifier = verifier;
  },
  [types.SET_CONFIRM_REQ](state, req) {
    state.phoneAuth.confirmRequest = req;
  }
};

export const AuthModule: Module<IAuthState, IRootState> = {
  namespaced: true,
  state: {
    user: null,
    token: null,
    isAuthed: false,
    phoneAuth: {
      verifier: null,
      confirmRequest: null
    }
  },
  actions,
  getters,
  mutations
};
