import { auth } from 'firebase';
import ConfirmationResult = auth.ConfirmationResult;
import ApplicationVerifier = auth.ApplicationVerifier;

export interface IAuthState {
  isAuthed: boolean;
  token: string | null;
  phoneAuth: {
    verifier: ApplicationVerifier | null
    confirmRequest: ConfirmationResult | null
  };
  user: {
    id: string
    uid: string
    phone: string
    email: string
    username: string
  } | null;
}
