export const MN_NOTIFICATIONS = 'notifications';
export const SET_IS_ERROR = 'SET_IS_ERROR';
export const SET_ACTIVE = 'SET_ACTIVE';
export const SET_MESSAGE = 'SET_MESSAGE';
