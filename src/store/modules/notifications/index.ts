import Switch from './switch';
import { IRootState } from '@/store/lib';
import * as types from './mutation.types';
import { INoticeState } from './notice.state';
import { ActionTree, GetterTree, MutationTree } from 'vuex';

export { MN_NOTIFICATIONS } from './mutation.types';
export { INoticeState };

const state: INoticeState = {
  active: false,
  message: null,
  isError: false
};

const getters: GetterTree<INoticeState, IRootState> = {
  isError: ({isError}) => isError,
  active: ({active}) => active,
  message: ({message}) => message
};

const actions: ActionTree<INoticeState, IRootState> = {
  handler({commit}, {isError = false, notice}) {
    return new Promise(resolve => {
      let message = notice, active = true;

      if (isError) {
        message = Switch(notice);
      }

      commit(types.SET_ACTIVE, active);
      commit(types.SET_MESSAGE, message);
      commit(types.SET_IS_ERROR, isError);

      const noticeTime = 3000;
      // Reset values
      setTimeout(() => commit(types.SET_ACTIVE, false), isError ? 4700 : 3000);
      setTimeout(() => {
        commit(types.SET_MESSAGE, null);
        resolve();
      }, isError ? 5000 : noticeTime + 300);
      setTimeout(() => commit(types.SET_IS_ERROR, false), 5200);
    });
  }
};

const mutations: MutationTree<INoticeState> = {
  [types.SET_ACTIVE](state, value) {
    state.active = value;
  },
  [types.SET_MESSAGE](state, value) {
    state.message = value;
  },
  [types.SET_IS_ERROR](state, value) {
    state.isError = value;
  }
};

export const NotificationsModule = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
