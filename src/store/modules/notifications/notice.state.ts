export interface INoticeState {
  message: any;
  active: boolean;
  isError: boolean;
}
