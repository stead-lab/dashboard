export default (err: any) => {
  if (process.env.NODE_ENV !== ' production') {
    console.error(err);
  } else console.debug(err);

  let errH = '';

  if (err?.code) {
    switch (err.code) {
      case 'auth/user-not-found':
      case 'auth/user-disabled':
      case 'auth/user-token-expired':
        errH = 'No such account exists with us.';
        break;
      case 'auth/invalid-email':
      case 'auth/wrong-password':
        errH = 'Wrong email or password';
        break;
      case 'auth/weak-password':
        errH = 'Please improve your passwords strength';
        break;
      case 'auth/email-already-in-use':
        errH = 'That email is already in use';
        break;
      case 'auth/network-request-failed':
        errH = 'A network issue occurred.';
        break;
      case 'auth/web-storage-unsupported':
        errH = 'An issue occurred with your browser';
        break;
      case 'auth/invalid-verification-code':
        errH = 'Invalid SMS validation code';
        break;
      case 'auth/internal-error':
        errH = 'An internal / network issue occurred';
        break;
      case 'auth/invalid-form':
        errH = err || 'There seems to be an issue with your input';
        break;
      default:
        errH = err || err;
    }
  } else if (err?.status === 500) {
    errH = 'An internal issue occurred';
  } else {
    errH = err?.message || err || 'An issue occurred';
  }

  return errH;
}
