import { Module } from 'vuex';
import { IFileState } from './file.state';
import { $delete, $put, client } from '@/services';
import { BaseActions, BaseState, CrudGetters, CrudMutations, IRootState } from '@/store/lib';

export { MN_FILES } from './mutation.types';

const getters = CrudGetters<IFileState, IRootState>();

const actions = BaseActions<IFileState, IRootState>(
  {
    client,
    endpoint: 'files'
  },
  {
    bulkCreate({commit, dispatch}, list) {
      const uploadList: Array<Promise<any>> = [];
      list.forEach((file: { name: string, file: File | string, tags: string[] }) => {
        uploadList.push(dispatch('create', file));
      });
      return Promise.all(uploadList);
    },
    create({commit}, {name, file, tags}) {
      const body = new FormData();
      body.append('name', name);
      body.append('file', file);
      body.append('tags', tags);
      return $put('files', body, {isFile: true});
    },
    delete({commit}, id) {
      return $delete(`files/${id}`);
    }
  }
);

const mutations = CrudMutations<IFileState, IRootState>();

export const FilesModule: Module<IFileState, IRootState> = {
  namespaced: true,
  state: BaseState(),
  actions,
  getters,
  mutations
};
