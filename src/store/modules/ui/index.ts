import * as types from './mutation.types';
import { IUiState } from './ui.state';
import { IRootState } from '@/store/lib';
import { ActionTree, GetterTree, MutationTree } from 'vuex';

export { MN_UI } from './mutation.types';

const state: IUiState = {
  loader: false,
  inviteState: 'Validating Your Invitation'
};

const getters: GetterTree<IUiState, IRootState> = {
  loader: ({loader}) => loader,
  inviteState: ({inviteState}) => inviteState
};

const actions: ActionTree<IUiState, IRootState> = {
  toggleLoader({dispatch}, value?: boolean) {
    dispatch('generalToggle', {key: 'loader', value});
  },
  toggleQuickAct({dispatch}, value?: boolean) {
    dispatch('generalToggle', {key: 'quickAct', value});
  },
  setInvState({commit}, value: string) {
    commit(types.GENERAL_SET, {key: 'inviteState', value});
  },
  generalToggle({commit, state}, {key, value}: { key: keyof IUiState, value?: any }) {
    let condition = {key, value};
    if (value === undefined || typeof value !== 'boolean') {
      condition.value = !state[key];
    }
    commit(types.GENERAL_SET, condition);
  },
  reset({commit}) {
    commit(types.RESET);
  }
};

const mutations: MutationTree<IUiState> = {
  [types.GENERAL_SET](state, {key, value}: { key: keyof IUiState, value: any }) {
    // @ts-ignore
    state[key] = value;
  },
  [types.RESET](state) {
    state.loader = false;
  }
};

export const UiModule = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
