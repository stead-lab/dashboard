import { SLS } from '@stead/sls';

export const Storage = new SLS({
  encodingType: 'aes',
  encryptionNamespace: 'stead',
  encryptionSecret: '5t34d-10'
});
