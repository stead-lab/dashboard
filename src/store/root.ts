import { IRootState } from '@/store/lib';
import { IAuthState } from './modules/auth/auth.state';
import { ActionTree, GetterTree, MutationTree } from 'vuex';

declare const mixpanel: {
  identify: (id: string) => void;
  people: {
    set: (obj: any) => void
  };
};

const types = {
  INIT_STORE: 'INIT_STORE'
};

const state: IRootState = {
  config: {},
  version: process.env.VERSION || '0.0.1',
  initialized: false
};

const getters: GetterTree<IRootState, IRootState> = {
  initialized: ({initialized}) => initialized
};

const actions: ActionTree<IRootState, IRootState> = {
  load({state, commit, dispatch}) {
    if (!state.initialized) {
      return dispatch('auth/load', {root: true})
        .then(() => console.debug('FIRE SUBSEQUENT MODULE LOADS'))
        .then(() => commit(types.INIT_STORE))
        .then(() => {
          if (process.env.NODE_ENV === 'production' && typeof mixpanel !== 'undefined') {
            const {user} = state.auth as IAuthState;
            if (user) {
              mixpanel.identify(user.id);
              mixpanel.people.set({
                username: user.username,
                $email: user.email,
                $last_seen: Date.now()
              });
            }
          }
        });
    }
  }
};

const mutations: MutationTree<IRootState> = {
  [types.INIT_STORE](state) {
    state.initialized = true;
  }
};

export const RootModule = {
  state,
  getters,
  actions,
  mutations
};
