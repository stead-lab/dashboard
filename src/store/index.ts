import Vue from 'vue';
import Vuex from 'vuex';
import { Storage } from './lib';
import PersistPlugin from 'vuex-persistedstate';

// STORE MODULES
import { RootModule } from './root';
import { UiModule } from './modules/ui';
import { AuthModule } from './modules/auth';
import { FilesModule } from './modules/files';
import { HorsesModule } from './modules/horses';
import { NotificationsModule } from './modules/notifications';

Vue.use(Vuex);

export default new Vuex.Store({
  ...RootModule,
  modules: {
    ui: UiModule,
    auth: AuthModule,
    files: FilesModule,
    horses: HorsesModule,
    notifications: NotificationsModule
  },
  plugins: [
    PersistPlugin({
      paths: ['horses'],
      storage: {
        getItem: (k) => Storage.get(k),
        setItem: (k, v) => Storage.set(k, v),
        removeItem: (k) => Storage.remove(k)
      }
    })
  ]
});
