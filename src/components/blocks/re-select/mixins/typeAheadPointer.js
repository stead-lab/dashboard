import Vue from 'vue';

export default Vue.extend({
  data() {
    return {
      phoneRx: /^(?:256|254)\d{9}$/,
      emailRx: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      typeAheadPointer: -1
    };
  },

  watch: {
    filteredOptions() {
      this.typeAheadPointer = 0;
    }
  },

  methods: {
    typeAheadPress(ev) {
      // Fixes issue with android mobile not
      // triggering a keypress instead falls to keyup event
      if (this.isMobile && ev.type === 'keypress') {
        return;
      }

      // Ignored keys
      if (['Shift', 'Ctrl', 'ArrowDown', 'ArrowUp'].includes(ev.key)) return;

      const value = ev.target.value;
      if (this.commaSplit) {
        if (value.split('').pop() === ',') {
          this.search = this.search.replace(/,/g, '').trim();
          let valid = true;

          if (this.isEmail) {
            valid = this.emailRx.test(this.search);
          }

          if (this.isPhone) {
            valid = this.phoneRx.test(this.search);
          }

          if (valid && this.search.length) {
            this.select(this.search);
            this.$refs.search.focus();
          }
        }
      } else if (this.keyedList) {
        const keys = Object.keys(this.keyedLists);
        keys.forEach(key => {
          if (value.startsWith(key)) {
            this.currentKey = key;
            this.initOptions(this.keyedLists[key]);
          }
        });
      }
    },
    /**
     * Move the typeAheadPointer visually up the list by
     * subtracting the current index by one.
     * @return {void}
     */
    typeAheadUp() {
      if (this.typeAheadPointer > 0) {
        this.typeAheadPointer--;
        if (this.maybeAdjustScroll) {
          this.maybeAdjustScroll();
        }
      }
    },

    /**
     * Move the typeAheadPointer visually down the list by
     * adding the current index by one.
     * @return {void}
     */
    typeAheadDown() {
      if (this.typeAheadPointer < this.filteredOptions.length - 1) {
        this.typeAheadPointer++;
        if (this.maybeAdjustScroll) {
          this.maybeAdjustScroll();
        }
      }
    },

    /**
     * Select the option at the current typeAheadPointer position.
     * Optionally clear the search input on selection.
     * @return {void}
     */
    typeAheadSelect() {
      if (this.filteredOptions[this.typeAheadPointer]) {
        this.select(this.filteredOptions[this.typeAheadPointer]);
      } else if (this.taggable && this.search.length) {
        this.select(this.search.trim());
      }

      if (this.clearSearchOnSelect) {
        this.search = '';
      }
    }
  }
});
