import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/functions'

const app = firebase.initializeApp({
  apiKey: '<stead_api_key>',
  databaseURL: 'https://<stead>.firebaseio.com',
  authDomain: '<stead>.firebaseapp.com',
  projectId: '<stead>'
})

export const FireAuth = app.auth()
export const FireFuncs = app.functions('europe-west1')

export const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export const fire = firebase;
