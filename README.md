# [Dashboard](https://gitlab.com/stead-lab/dashboard)
[![pipeline status](https://gitlab.com/stead-lab/dashboard/badges/master/pipeline.svg)](https://gitlab.com/stead-lab/dashboard)
> Dashboard Starter in Vue [opinionated]

A shell like setup to start out a dashboard built in vue


## Setup
```bash
yarn install
```

```bash
yarn run serve
```

```bash
yarn run build
```
