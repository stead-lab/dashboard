module.exports = {
  productionSourceMap: false,
  //   }
  devServer: {
    host: '0.0.0.0',
    port: 8090,
    hot: true,
    disableHostCheck: true
  },
  chainWebpack: (config) => {
    config
      .plugin('define')
      .tap((args) => {
        args[0]['process.env'] = Object.assign(args[0]['process.env'], {
          VERSION: JSON.stringify(require('./package.json').version),
          SENTRY_DSN: '\'https://<project_dsn>@sentry.io/<and_this>\''
        });
        return args;
      });
  },
  // pwa: {
  //   // WORKBOX SETTINGS
  //   workboxPluginMode: 'InjectManifest',
  //   workboxOptions: {
  //     swSrc: './src/sw/index.js',
  //     swDest: 'sw.js'
  // }
};
